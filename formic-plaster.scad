include <laser.scad>;

width = 150;
height = 100;
depth = 200;
nest_depth = 20;
def_offset = 7;
corr_h = 10;
v_corr_h = 12;

outer_offset = 7;

top_hole_w = width - 50;
top_hole_h = depth - 50;
top_hole_r = 10;

cap_offset = 10;

thickness = 3;

screw_dia = 3;
front_screw_dia = 4;

handle_dia = 40;
handle_stem_w = 12;

vent_h = 0.1;
vent_g = 3;

side_vents_h = 12;
side_vents_w = 50;

top_vents_h = 12;
top_vents_w = top_hole_w / 2;

portal_dia = 11.5;
port_dia = 10;

humidifier_w = 10;
humidifier_port_d = 2.6;

nx = 3;
ny = 4;

sensor_h = 12;
sensor_w = 6;
sensor_wall_pin_d = 0.3;
sensor_wall_off = 2;
sensor_wall_pin_gap = 1.5;

gate_pin_d = 2;
gate_h = 16;

module vents(w,h) {
    for (i=[0:h/vent_g]) {
        translate([0, i*vent_g]) square([w, vent_h]);
    }
}

module outworld_bottom() {
    difference() {
        rounded_square([thickness*2 + width + thickness*2, thickness*2 + nest_depth + thickness*2 + depth + thickness*2 + nest_depth + thickness*2]);
        translate([width/8 - oring_slot_width/2 + thickness*2, thickness*2 + nest_depth + thickness]) oring_joint_slot();
        translate([7*width/8 - oring_slot_width/2 + thickness*2, thickness*2 + nest_depth + thickness]) oring_joint_slot();
        translate([width/8 - oring_slot_width/2 + thickness*2, thickness*2 + nest_depth + thickness*2 + depth]) oring_joint_slot();
        translate([7*width/8 - oring_slot_width/2 + thickness*2, thickness*2 + nest_depth + thickness*2 + depth]) oring_joint_slot();

        translate([width/8 - oring_slot_width/2 + thickness*2, thickness]) oring_joint_slot();
        translate([7*width/8 - oring_slot_width/2 + thickness*2, thickness]) oring_joint_slot();
        translate([width/8 - oring_slot_width/2 + thickness*2, thickness*2 + nest_depth + thickness*2 + depth + thickness*2 + nest_depth]) oring_joint_slot();
        translate([7*width/8 - oring_slot_width/2 + thickness*2, thickness*2 + nest_depth + thickness*2 + depth + thickness*2 + nest_depth]) oring_joint_slot();

        translate([thickness*2, thickness*2 + nest_depth + thickness*2 + depth/8 - oring_slot_width/2]) rotate(90) oring_joint_slot();
        translate([thickness*2, thickness*2 + nest_depth + thickness*2 + 7*depth/8 - oring_slot_width/2]) rotate(90) oring_joint_slot();
        translate([thickness*2 + width + thickness, thickness*2 + nest_depth + thickness*2 + depth/8 - oring_slot_width/2]) rotate(90) oring_joint_slot();
        translate([thickness*2 + width + thickness, thickness*2 + nest_depth + thickness*2 + 7*depth/8 - oring_slot_width/2]) rotate(90) oring_joint_slot();

        translate([thickness*2, thickness*2 + (nest_depth + thickness)/2 - oring_slot_width/2]) rotate(90) oring_joint_slot();
        translate([thickness*2 + width + thickness, thickness*2 + (nest_depth + thickness)/2 - oring_slot_width/2]) rotate(90) oring_joint_slot();
        translate([thickness*2, thickness*2 + nest_depth + thickness*2 + depth + thickness + (nest_depth + thickness)/2 - oring_slot_width/2]) rotate(90) oring_joint_slot();
        translate([thickness*2 + width + thickness, thickness*2 + nest_depth + thickness*2 + depth + thickness + (nest_depth + thickness)/2 - oring_slot_width/2]) rotate(90) oring_joint_slot();
    }
}

module outworld_side() {
    union() {
        difference() {
            rounded_square([height, thickness*2 + nest_depth + thickness*2 + depth + thickness*2 + nest_depth + thickness*2]);
            translate([height - thickness, thickness*2 + nest_depth/4]) square([thickness, nest_depth/2]);
            translate([height - thickness, thickness*2 + nest_depth + thickness*2 + depth + thickness*2 + nest_depth/4]) square([thickness, nest_depth/2]);
            translate([height - thickness, thickness*2 + nest_depth + thickness*2 + depth/8]) square([thickness, 6*depth/8]);

            translate([height/4 - oring_slot_width/2, thickness*2 + nest_depth + thickness]) oring_joint_slot();
            translate([3*height/4 - oring_slot_width/2, thickness*2 + nest_depth + thickness]) oring_joint_slot();
            translate([height/4 - oring_slot_width/2, thickness*2 + nest_depth + 2*thickness + depth]) oring_joint_slot();
            translate([3*height/4 - oring_slot_width/2, thickness*2 + nest_depth + 2*thickness + depth]) oring_joint_slot();

            translate([outer_offset, thickness*2 + nest_depth + thickness*2 + outer_offset]) mirror([1,0,0]) rotate(90) vents(side_vents_w, side_vents_h);
            translate([outer_offset, thickness*2 + nest_depth + thickness*2 + depth - side_vents_w - outer_offset]) mirror([1,0,0]) rotate(90) vents(side_vents_w, side_vents_h);
        }
        translate([0, thickness*2 + nest_depth + thickness*2 + depth/8 - oring_slot_width/2]) rotate(90) oring_joint_plug();
        translate([0, thickness*2 + nest_depth + thickness*2 + 7*depth/8 - oring_slot_width/2]) rotate(90) oring_joint_plug();

        translate([0, thickness*2 + (nest_depth + thickness)/2 - oring_slot_width/2]) rotate(90) oring_joint_plug();
        translate([0, thickness*2 + nest_depth + thickness*2 + depth + thickness + (nest_depth + thickness)/2 - oring_slot_width/2]) rotate(90) oring_joint_plug();
    }
}

module outworld_front() {
    union() {
        difference() {
            square([width, height]);
            translate([width/8, height - thickness]) square([6*width/8, thickness]);
            translate([width/2, outer_offset + portal_dia/2]) circle(d=portal_dia);
        }
        translate([width/8 - oring_slot_width/2, thickness]) mirror([0,1,0]) oring_joint_plug();
        translate([7*width/8 - oring_slot_width/2, thickness]) mirror([0,1,0]) oring_joint_plug();

        translate([0, height/4 - oring_slot_width/2]) rotate(90) oring_joint_plug();
        translate([0, 3*height/4 - oring_slot_width/2]) rotate(90) oring_joint_plug();
        translate([width, height/4 - oring_slot_width/2]) translate([0, oring_slot_width]) rotate(-90) oring_joint_plug();
        translate([width, 3*height/4 - oring_slot_width/2]) translate([0, oring_slot_width]) rotate(-90) oring_joint_plug();
    }
}

module outworld_top() {
    difference() {
        square([width + 2*thickness, depth + 2*thickness]);
        translate([0, 0]) square([width/8 + thickness, thickness]);
        translate([width + thickness - width/8, 0]) square([width/8 + thickness, thickness]);
        translate([0, depth + thickness]) square([width/8 + thickness, thickness]);
        translate([width + thickness - width/8, depth + thickness]) square([width/8 + thickness, thickness]);
        translate([0, 0]) square([thickness, depth/8 + thickness]);
        translate([width + thickness, 0]) square([thickness, depth/8 + thickness]);
        translate([0, depth + thickness - depth/8]) square([thickness, depth/8 + thickness]);
        translate([width + thickness, depth + thickness - depth/8]) square([thickness, depth/8 + thickness]);
        translate([thickness + width/2, thickness + depth/2]) rounded_square([top_hole_w, top_hole_h], r=top_hole_r, center=true);
    }
    translate([width/2 + thickness, depth/2 + thickness]) square([thickness, handle_dia], center=true);
    translate([top_hole_w/2 - 5*screw_dia, top_hole_h/2 - 5*screw_dia]) translate([width/2+thickness, depth/2+thickness]) circle(d=screw_dia);
    translate([-(top_hole_w/2 - 5*screw_dia), top_hole_h/2 - 5*screw_dia]) translate([width/2+thickness, depth/2+thickness]) circle(d=screw_dia);
    translate([top_hole_w/2 - 5*screw_dia, -(top_hole_h/2 - 5*screw_dia)]) translate([width/2+thickness, depth/2+thickness]) circle(d=screw_dia);
    translate([-(top_hole_w/2 - 5*screw_dia), -(top_hole_h/2 - 5*screw_dia)]) translate([width/2+thickness, depth/2+thickness]) circle(d=screw_dia);
    translate([0, top_hole_h/2 - 5*screw_dia]) translate([width/2+thickness, depth/2+thickness]) translate([-top_vents_w/2, -top_vents_h/2]) vents(top_vents_w, top_vents_h);
    translate([0, -(top_hole_h/2 - 5*screw_dia)]) translate([width/2+thickness, depth/2+thickness]) translate([-top_vents_w/2, -top_vents_h/2]) vents(top_vents_w, top_vents_h);
}

module outworld_cap() {
    difference() {
        rounded_square([top_hole_w + cap_offset*2, top_hole_h + cap_offset*2], r=top_hole_r);
        translate([(top_hole_w + cap_offset*2)/2, (top_hole_h + cap_offset*2)/2]) square([handle_dia, thickness], center=true);
        translate([(top_hole_w + cap_offset*2)/2, (top_hole_h + cap_offset*2)/2]) circle(d=handle_stem_w);
        translate([top_hole_w/2 - 5*screw_dia, top_hole_h/2 - 5*screw_dia]) translate([(top_hole_w + cap_offset*2)/2, (top_hole_h + cap_offset*2)/2]) circle(d=screw_dia);
        translate([-(top_hole_w/2 - 5*screw_dia), top_hole_h/2 - 5*screw_dia]) translate([(top_hole_w + cap_offset*2)/2, (top_hole_h + cap_offset*2)/2]) circle(d=screw_dia);
        translate([top_hole_w/2 - 5*screw_dia, -(top_hole_h/2 - 5*screw_dia)]) translate([(top_hole_w + cap_offset*2)/2, (top_hole_h + cap_offset*2)/2]) circle(d=screw_dia);
        translate([-(top_hole_w/2 - 5*screw_dia), -(top_hole_h/2 - 5*screw_dia)]) translate([(top_hole_w + cap_offset*2)/2, (top_hole_h + cap_offset*2)/2]) circle(d=screw_dia);

        translate([0, top_hole_h/2 - 5*screw_dia]) translate([(top_hole_w + cap_offset*2)/2, (top_hole_h + cap_offset*2)/2]) translate([-top_vents_w/2, -top_vents_h/2]) vents(top_vents_w, top_vents_h);
        translate([0, -(top_hole_h/2 - 5*screw_dia)]) translate([(top_hole_w + cap_offset*2)/2, (top_hole_h + cap_offset*2)/2]) translate([-top_vents_w/2, -top_vents_h/2]) vents(top_vents_w, top_vents_h);
    }
}

module outworld_handle() {
    union() {
        difference() {
            circle(d=handle_dia);
            translate([-handle_dia/2, -handle_dia]) square([handle_dia,handle_dia]);
        }
        translate([0, -thickness/2]) square([handle_stem_w, thickness], center=true);
        translate([0, -thickness/2 - thickness]) square([handle_dia, thickness], center=true);
    }
}

module nest_front() {
    union() {
        difference() {
            square([width - 2*thickness, height - 2*thickness]);
            //translate([(width - 2*thickness)/2, outer_offset + portal_dia/2]) circle(d=portal_dia);
            r_w = (width - 2*thickness - 2*def_offset - 2*outer_offset - 2*humidifier_w + def_offset) / nx - def_offset;
            r_h = (height - 2*thickness - 2*outer_offset + def_offset) / ny - def_offset;
            for (j=[0:ny-1]) {
                if (j != ny-1) translate([outer_offset + humidifier_w + def_offset + r_w - v_corr_h - 2, outer_offset + r_h + j*(r_h + def_offset) - thickness/2 + def_offset/2 ]) square([v_corr_h, thickness]);
            }
            translate([outer_offset + humidifier_w + def_offset + sensor_w, outer_offset + laser_adjust/2]) square([thickness, r_h - laser_adjust]);
            translate([outer_offset + humidifier_w + def_offset, outer_offset + r_h/2 - sensor_h/2]) square([sensor_w, sensor_h]);
            translate([width - 2*thickness - (outer_offset + humidifier_w + def_offset + sensor_w) - thickness, outer_offset + laser_adjust/2]) square([thickness, r_h - laser_adjust]);
            translate([width - 2*thickness - (outer_offset + humidifier_w + def_offset) - sensor_w, outer_offset + r_h/2 - sensor_h/2]) square([sensor_w, sensor_h]);
            translate([outer_offset + humidifier_w + def_offset + sensor_w, height - 2*thickness - (outer_offset - laser_adjust/2) - r_h]) square([thickness, r_h - laser_adjust]);
            translate([outer_offset + humidifier_w + def_offset, height - 2*thickness - (outer_offset + r_h/2 - sensor_h/2) - sensor_h]) square([sensor_w, sensor_h]);
            translate([width - 2*thickness - (outer_offset + humidifier_w + def_offset + sensor_w) - thickness, height - 2*thickness - (outer_offset - laser_adjust/2) - r_h]) square([thickness, r_h - laser_adjust]);
            translate([width - 2*thickness - (outer_offset + humidifier_w + def_offset) - sensor_w, height - 2*thickness - (outer_offset + r_h/2 - sensor_h/2) - sensor_h]) square([sensor_w, sensor_h]);

            translate([outer_offset + humidifier_w + def_offset + 0*(r_w + def_offset) + r_w + def_offset/2, outer_offset + 0*(r_h + def_offset) + r_h + def_offset/2]) circle(d=front_screw_dia);
            translate([outer_offset + humidifier_w + def_offset + (nx-2)*(r_w + def_offset) + r_w + def_offset/2, outer_offset + 0*(r_h + def_offset) + r_h + def_offset/2]) circle(d=front_screw_dia);
            translate([outer_offset + humidifier_w + def_offset + 0*(r_w + def_offset) + r_w + def_offset/2, outer_offset + (ny-2)*(r_h + def_offset) + r_h + def_offset/2]) circle(d=front_screw_dia);
            translate([outer_offset + humidifier_w + def_offset + (nx-2)*(r_w + def_offset) + r_w + def_offset/2, outer_offset + (ny-2)*(r_h + def_offset) + r_h + def_offset/2]) circle(d=front_screw_dia);

            translate([outer_offset + humidifier_w/2, outer_offset + def_offset]) circle(d=humidifier_port_d);
            translate([width - 2*thickness - (outer_offset + humidifier_w/2), outer_offset + def_offset]) circle(d=humidifier_port_d);
            translate([outer_offset + humidifier_w/2, height - 2*thickness - (outer_offset + def_offset)]) circle(d=humidifier_port_d);
            translate([width - 2*thickness - (outer_offset + humidifier_w/2), height - 2*thickness - (outer_offset + def_offset)]) circle(d=humidifier_port_d);
        }
        translate([width/8 - oring_slot_width/2 - thickness, 0]) mirror([0,1,0]) oring_joint_plug();
        translate([7*width/8 - oring_slot_width/2 - thickness, 0]) mirror([0,1,0]) oring_joint_plug();
        //translate([0, -thickness]) fingers(thickness, width - 2*thickness, 5);
        translate([0, height-2*thickness]) fingers(thickness, width - 2*thickness, 5);
        translate([0, 0]) rotate(90) fingers(thickness, height - 2*thickness, 4);
        translate([width-thickness, 0]) rotate(90) fingers(thickness, height - 2*thickness, 4);
    }
}

module nest_top() {
    union() {
        difference() {
            square([width - 2*thickness, nest_depth + thickness]);
            translate([(width - 2*thickness)/2, nest_depth/2]) circle(d=portal_dia);
            translate([outer_offset + humidifier_w/2, nest_depth/2]) circle(d=port_dia);
            translate([width - 2*thickness - (outer_offset + humidifier_w/2), nest_depth/2]) circle(d=port_dia);
        }
        translate([-2*thickness, nest_depth/4 - laser_adjust/2]) square([2*thickness, nest_depth/2 + laser_adjust]);
        translate([width - 2*thickness, nest_depth/4 - laser_adjust/2]) square([2*thickness, nest_depth/2 + laser_adjust]);
        translate([0, -thickness]) fingers(thickness, width - 2*thickness, 5, parity=true);
    }
}

module nest_bottom() {
    union() {
        square([width - 2*thickness, nest_depth + thickness]);
        translate([-thickness, nest_depth/4 - laser_adjust/2 + thickness]) square([thickness, nest_depth/2 + laser_adjust]);
        translate([width - 2*thickness, nest_depth/4 - laser_adjust/2 + thickness]) square([thickness, nest_depth/2 + laser_adjust]);
        //translate([0, nest_depth + thickness]) fingers(thickness, width - 2*thickness, 5, parity=true);
        difference() {
            translate([0, nest_depth + thickness]) square([width - 2*thickness, thickness]);
            translate([width/8 - oring_slot_width/2 - thickness + laser_adjust/2, nest_depth + thickness]) square([oring_slot_width - laser_adjust, thickness]);
            translate([7*width/8 - oring_slot_width/2 - thickness + laser_adjust/2, nest_depth + thickness]) square([oring_slot_width - laser_adjust, thickness]);
        }
    }
}

module nest_side() {
    union() {
        difference() {
            translate([0, -thickness]) square([nest_depth + thickness, height]);
            translate([nest_depth/4 + laser_adjust/2, -thickness]) square([nest_depth/2 - laser_adjust, thickness]);
            translate([nest_depth/4 + laser_adjust/2, height - 2*thickness]) square([nest_depth/2 - laser_adjust, thickness]);
        }
        translate([0, 0]) rotate(90) fingers(thickness, height - 2*thickness, 4, parity=true);
    }
}

module nest_layer_1() {
    translate([outer_offset, outer_offset]) rounded_square([humidifier_w, height - 2*thickness - outer_offset]);
    translate([width - 2*thickness - outer_offset - humidifier_w, outer_offset]) rounded_square([humidifier_w, height - 2*thickness - outer_offset]);

    r_w = (width - 2*thickness - 2*def_offset - 2*outer_offset - 2*humidifier_w + def_offset) / nx - def_offset;
    r_h = (height - 2*thickness - 2*outer_offset + def_offset) / ny - def_offset;
    echo(r_w);
    echo(r_h);
    for (j=[0:ny-1]) {
        for (i=[0:nx-1]) {
            translate([outer_offset + humidifier_w + def_offset + i*(r_w + def_offset), outer_offset + j*(r_h + def_offset)]) rounded_square([r_w, r_h]);
            if (i != nx-1) translate([outer_offset + humidifier_w + def_offset + i*(r_w + def_offset) + r_w, outer_offset + j*(r_h + def_offset)+2]) square([def_offset, corr_h]);
        }
        if (j != ny-1) translate([outer_offset + humidifier_w + def_offset + r_w - v_corr_h - 2, outer_offset + r_h + j*(r_h + def_offset)]) square([v_corr_h, def_offset]);
    }
}

module nest_layer_2() {
    translate([outer_offset, outer_offset]) rounded_square([humidifier_w, height - 2*thickness - outer_offset]);
    translate([width - 2*thickness - outer_offset - humidifier_w, outer_offset]) rounded_square([humidifier_w, height - 2*thickness - outer_offset]);

    r_w = (width - 2*thickness - 2*def_offset - 2*outer_offset - 2*humidifier_w + def_offset) / nx - def_offset;
    r_h = (height - 2*thickness - 2*outer_offset + def_offset) / ny - def_offset;
    echo(r_w);
    echo(r_h);
    for (j=[0:ny-1]) {
        for (i=[0:nx-1]) {
            translate([outer_offset + humidifier_w + def_offset + i*(r_w + def_offset), outer_offset + j*(r_h + def_offset)]) rounded_square([r_w, r_h]);
        }
    }
}

module gate_closed() {
    union() {
        translate([(v_corr_h + laser_adjust)/2, gate_h]) difference() {
            circle(d = v_corr_h + laser_adjust);
            translate([0, (v_corr_h + laser_adjust) * 3/12]) circle(d = gate_pin_d);
        }
        square([v_corr_h + laser_adjust, gate_h]);
    }
}

module gate_open() {
    difference() {
        union() {
            translate([-laser_adjust/2, 0]) square([v_corr_h + laser_adjust, thickness + v_corr_h/3]);
            translate([-thickness, thickness + v_corr_h/3]) square([v_corr_h + thickness*2, thickness]);
        }
        translate([v_corr_h/2, -thickness]) circle(d = v_corr_h);
    }
}

module sensor_wall() {
    difference() {
        union() {
            translate([-laser_adjust/2, 0]) square([sensor_h + laser_adjust, gate_h+thickness]);
            translate([-thickness, gate_h + thickness]) square([sensor_h + thickness*2, thickness]);
        }
        nx = ceil((sensor_h - 2*sensor_wall_off) / sensor_wall_pin_gap);
        real_off_x = (sensor_h - sensor_wall_pin_gap * nx)/2;
        ny = ceil((gate_h - 2*sensor_wall_off) / sensor_wall_pin_gap);
        real_off_y = (gate_h - sensor_wall_pin_gap * ny)/2;
        for(i = [0:nx]) {
            for(j = [0:ny]) {
                translate([real_off_x + i*sensor_wall_pin_gap, real_off_y + j*sensor_wall_pin_gap]) circle(d=sensor_wall_pin_d);
            }
        }
    }
}
/*
outworld_bottom();
translate([thickness*2 + width + thickness*2 + 10, 0]) outworld_side();
translate([-(thickness + height + thickness + 10), 0]) translate([height, 0]) mirror([1,0,0]) outworld_side();
translate([thickness*2, thickness*2 + nest_depth + thickness*2 + depth + thickness*2 + nest_depth + thickness*2 + 10]) outworld_front();
translate([thickness*2, -10]) mirror([0,1,0]) outworld_front();
//translate([thickness, -10 - height - 10 - depth]) outworld_top();
translate([thickness*2 + width + thickness*2 + 10 + height + 10, thickness*2 + nest_depth + thickness]) outworld_top();
translate([thickness*2 + width + thickness*2 + 10 + height + 10 + width + thickness*2 + 10, thickness*2 + nest_depth + thickness]) outworld_cap();
translate([thickness*2 + width + thickness*2 + 10 + height + 10 + width + thickness*2 + 10 + width + 10, thickness*2 + nest_depth + thickness]) outworld_handle();
translate([-400, 0]) {
    difference() {
        nest_front();
 //       nest_layer_1();
    }
    translate([0, height + 10]) nest_top();
    translate([0, -nest_depth - 15]) nest_bottom();
    translate([-10, 0]) mirror([1,0,0]) nest_side();
    translate([width + 10, 0]) nest_side();
    translate([width + 10 + nest_depth + 10, 0]) gate_closed();
    translate([width + 10 + nest_depth + 10, gate_h + 10]) gate_open();
    translate([width + 10 + nest_depth + 10, (gate_h + 10)*2]) sensor_wall();
}*/

difference() {
    linear_extrude(height=8) nest_layer_1();
    translate([outer_offset + humidifier_w/2, outer_offset + def_offset]) cylinder(d=humidifier_port_d, h=thickness);
    translate([width - 2*thickness - (outer_offset + humidifier_w/2), outer_offset + def_offset]) cylinder(d=humidifier_port_d, h=thickness);
    translate([outer_offset + humidifier_w/2, height - 2*thickness - (outer_offset + def_offset)]) cylinder(d=humidifier_port_d, h=thickness);
    translate([width - 2*thickness - (outer_offset + humidifier_w/2), height - 2*thickness - (outer_offset + def_offset)]) cylinder(d=humidifier_port_d, h=thickness);
    linear_extrude(height = 3) {
            r_w = (width - 2*thickness - 2*def_offset - 2*outer_offset - 2*humidifier_w + def_offset) / nx - def_offset;
            r_h = (height - 2*thickness - 2*outer_offset + def_offset) / ny - def_offset;
            translate([outer_offset + humidifier_w + def_offset + (sensor_w - thickness), outer_offset + r_h/2 - sensor_h/2]) square([thickness, sensor_h]);
            translate([width - 2*thickness - (outer_offset + humidifier_w + def_offset) - sensor_w, outer_offset + r_h/2 - sensor_h/2]) square([thickness, sensor_h]);
            translate([outer_offset + humidifier_w + def_offset + (sensor_w - thickness), height - 2*thickness - (outer_offset + r_h/2 - sensor_h/2) - sensor_h]) square([thickness, sensor_h]);
            translate([width - 2*thickness - (outer_offset + humidifier_w + def_offset) - sensor_w, height - 2*thickness - (outer_offset + r_h/2 - sensor_h/2) - sensor_h]) square([thickness, sensor_h]);
    }
}
translate([0,0,8]) linear_extrude(height=8) nest_layer_2();
translate([(width - 2*thickness)/2, outer_offset + portal_dia/2, 16]) cylinder(d=portal_dia,h=7);
/*translate([(width - 2*thickness)/2, height - 7, nest_depth/2]) rotate([90,0,0]) cylinder(d=portal_dia, h=10);*/
