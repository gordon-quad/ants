thickness = 3; // material thickness
width = 50;
height = 30;
offset = 0.15; //TODO
space = 1;

plug_w 	= thickness*5;
plug_d 	= thickness;
plug_h 	= thickness;
bolt_d	= 3-offset*2;
bolt_l	= 20;
bolt_s 	= plug_w*0.5; // space around bolt in plug
nut_w	= 6;
nut_h 	= 3;
nut_o	= bolt_l - nut_h*2 - thickness; // nut offset

laser_adjust = 0.15;

$fs=0.6;$fa=5;$fn=60;

module test() {
    // Plate A
    difference() {
        union() {
            square([width,height]);
            translate([width,height/2]) conn_plug(plug_w,plug_d,rot=90);
        }
        translate([width,height/2]) {
            conn_plug_cutout(plug_d,bolt_d,bolt_l,nut_w,nut_h,nut_o,rot=90);
        }
    }
    // Plate B
    translate([0,height+space]) {
        difference() {
            square([width,height]);
            translate([width-thickness,height/2]) {
                conn_hole(plug_w,plug_h,bolt_d,rot=90,bolt_s=bolt_s);
            }
        }
    }
    // Plate C
    translate([0,2*(height+space)]) {
        difference() {
            square([width,height]);
            portal_outer_layer();
        }
    }
    // Plate C
    translate([0,3*(height+space)]) {
        difference() {
            square([width,height]);
            portal_middle_layer();
        }
        portal_outer_layer();
    }

    // Plate D
    translate([0,4*(height+space)]) {
        portal_plug();
    }
}

// plug pointing upwards, center: middle, bottom (f towards bottom)
module conn_plug(plug_w=plug_w,plug_d=plug_d,rot=0,f=1) {
	rotate([0,0,rot]) translate([-plug_w/2,-plug_d]) square([plug_w,plug_d+f]);
}
// plug cutout pointing upwards, center: middle, bottom (f towards top)
module conn_plug_cutout(plug_d=plug_d,bolt_d=bolt_d,bolt_l=bolt_l,nut_w=nut_w,nut_h=nut_h,nut_o=nut_o,rot=0,bolt_s=bolt_s,f=1) {
	rotate([0,0,rot]) union() {
		translate([-bolt_s/2,-plug_d-f])
			square([bolt_s,plug_d+f]);
		translate([-bolt_d/2,-plug_d,0])
			square([bolt_d,bolt_l]);
		translate([-nut_w/2,nut_o,0])
			square([nut_w,nut_h]);
	}
}
// hole
// origin: top center
module conn_hole(plug_w=plug_w,plug_h=plug_h,bolt_d=bolt_d,rot=0,bolt_s=8,f=1) {
	rotate([0,0,rot]) translate([0,plug_h/2,0]) {
		difference() {
			square([plug_w,plug_h],center=true);
			translate([0,-f,0]) square([bolt_s,plug_h+f*2],center=true);
		}
		circle(d=bolt_d);
	}
}

portal_d = 11.5;
//portal_w = 11.5;
//portal_h = 11.5;
//portal_r = 1.5;

portal_o = 10;
portal_plug_o1 = 1.25;

portal_width = portal_o + portal_d + portal_plug_o1;
portal_height = portal_plug_o1 + portal_d + portal_plug_o1;

module portal_outer_layer() {
    translate([portal_o + portal_d/2, portal_plug_o1 + portal_d/2]) circle(d=portal_d);
}

module portal_middle_layer() {
    union() {
        translate([portal_o + portal_d/2, portal_plug_o1 + portal_d/2]) circle(d=portal_d + 2*portal_plug_o1);
        square([portal_o + portal_d/2, portal_d + portal_plug_o1*2]);
    }
}

module portal_inner_layer() {
    union() {
        translate([portal_o + portal_d/2, portal_plug_o1 + portal_d/2]) circle(d=portal_d + 2*portal_plug_o1);
    }
}

module portal_plug() {
}

module rounded_square(wh, r=1.5, center=false) {
    wwh = is_list(wh) ? (wh - 2*[for(i = [1:len(wh)]) r]) : (wh - 2*r);
    if (center)
    {
        minkowski() {
            square(wwh, center=center);
            circle(r=r);
        }
    } else {
        translate([r,r]) minkowski() {
            square(wwh, center=center);
            circle(r=r);
        }
    }
}

oring_thickness = 2;
oring_slot_width = 20;

module oring_joint_slot(thickness = thickness, oring_thickness = oring_thickness, width = oring_slot_width) {
    square([width, thickness]);
}

module oring_joint_plug(thickness = thickness, oring_thickness = oring_thickness, width = oring_slot_width, r = 1, adj = 0.1) {
    height = thickness + oring_thickness + r - adj;
    difference() {
        union() {
            square([width, thickness + oring_thickness - adj]);
            translate([r, thickness + oring_thickness - adj]) minkowski() {
                square([width-2*r, 0.0001]);
                circle(r=r);
            }
        }
        translate([0, thickness - adj + oring_thickness/2]) circle(d = oring_thickness);
        translate([width, thickness - adj + oring_thickness/2]) circle(d = oring_thickness);
    }
}

module fingers(thickness, width, num, parity=false, tightness_adjust=laser_adjust) {
    finger_width = width / (2*num + 1);
    if (parity) {
        for (i = [0:num]) {
            translate([finger_width*2*i-tightness_adjust/2, 0]) square([finger_width + tightness_adjust+0.0001, thickness+0.0001]);
        }
    } else {
        for (i = [0:num-1]) {
            translate([finger_width + finger_width*2*i-tightness_adjust/2, 0]) square([finger_width + tightness_adjust+0.0001, thickness+0.0001]);
        }
    }
}

